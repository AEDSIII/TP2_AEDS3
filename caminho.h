#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
typedef struct Time{
  char *nome;
  int cidade;
}Time;

typedef struct Estadio{
  Time time;
  Time *times;
  int cidade;
  char *nome;
  int ocupado;
}Estadio;

Time *lerTime(FILE *arquivo, Time *time);

Estadio *lerCidade(FILE *arquivo, Estadio *estadio);

Estadio *pegaDados(Estadio *estadio, Time *time);

Estadio *vinculo(Estadio *estadio, Time *time);

int retornaDist(Estadio estadio1, Estadio estadio2);

void sorteio(Estadio *times);
